import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Community from './community';
import Join from './join';
import { Provider } from 'react-redux';
import store from './store/join';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={ store }>
    <React.StrictMode>
      <>
        <Community />
        <Join />
      </>
    </React.StrictMode>
  </Provider>
);

