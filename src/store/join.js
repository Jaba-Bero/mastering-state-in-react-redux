/* eslint-disable no-mixed-operators */
import { createStore } from 'redux';
import sendRequest from '../sendRequest';

const initialState = {button: 'SUBSCRIBE', toggle: true}
const subscribeReducer = (state = initialState, action) => {
    if (action.type === 'subscribe') {
        const input = document.getElementById('added-section__email-input');
        const email = input.value;
        const validEmailEndings = ['gmail.com', 'outlook.com', 'yandex.ru'];
        const ending = email.substring(email.indexOf('@') + 1);
        const start = email.substring(0, email.indexOf('@'));
        const form = document.getElementById('added-section__email-box');
        const button = document.getElementById('added-section__email-btn');
        if(ending === validEmailEndings[0] && start.length > 0 || 
            validEmailEndings[1] && start.length > 0 ||
            validEmailEndings[2] && start.length > 0) {

            }

        if(state.button === 'SUBSCRIBE') {
            if(ending === validEmailEndings[0] && start.length > 0 || 
                validEmailEndings[1] && start.length > 0 ||
                validEmailEndings[2] && start.length > 0) {
                 localStorage.setItem('email', email);
                 input.style.display = 'none';
                 button.style.height = '42px';
                 form.style.justifyContent = 'center';
                 sendRequest(email, 'http://localhost:3000/subscribe', button);
                 return {
                    button: 'UNSUBSCRIBE',
                    toggle: state.toggle
                 } 
                }else {
                    alert('false')
                }

        } else {
            input.style.display = 'inline-block';
            button.style.height = '42px';
            form.style.justifyContent = 'space-between';
            localStorage.removeItem('email');
            input.value = '';
            sendRequest(email, 'http://localhost:3000/unsubscribe', button);
            return {
                button: 'SUBSCRIBE',
                toggle: state.toggle
            } 
        }
    }

    if (action.type === 'toggle') {
        const section = document.getElementById("community-users");
        const btn = document.getElementById('btn')
        if(btn.innerHTML === 'Hide Section') {
          btn.innerHTML = "Show Section";
          section.classList.add('hide');
          return {
            toggle: !state.toggle,
            button: state.button
         }
        } else {
          btn.innerHTML = "Hide Section";
          section.classList.remove('hide');
          return {
            toggle: !state.toggle,
            button: state.button
         }
        }
    }

    return state
}

const store = createStore(subscribeReducer);

export default store